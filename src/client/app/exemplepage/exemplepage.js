(function () {
    'use strict';

    angular
        .module('app.exemplepage')
        .controller('Exemplepage', Exemplepage);

    /* @ngInject */
    function Exemplepage(dataservice, logger) {
        /*jshint validthis: true */
        var vm = this;
        vm.title = 'exemplepage Title';
        vm.something = [];

        //  $auth.logout();

        activate();

        function activate() {
            var promises = [];
            return dataservice.ready(promises).then(function () {
                logger.info('Exemple chargé');
            })
        }


        function getsomething() {
            return dataservice.getsomething().then(function (data) {
                vm.something = data;
                return vm.something;
            });
        }

    }
})();