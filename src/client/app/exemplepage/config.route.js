(function() {
    'use strict';

    angular
        .module('app.exemplepage')
        .run(appRun);

    // appRun.$inject = ['routehelper']

    /* @ngInject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/exemplepage',
                config: {
                    templateUrl: 'app/exemplepage/exemplepage.html',
                    controller: 'Exemplepage',
                    controllerAs: 'vm',
                    title: 'Exemplepage',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-plus-circle"></i> Exemplepage'
                    }
                }
            }
        ];
    }
})();
